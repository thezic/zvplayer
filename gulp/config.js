const path = require('path');

const root = path.join(__dirname, '..');
const app_root = path.join(root, 'app');
const build_root = path.join(root, 'build');

module.exports = {
  path: {
    sass: path.join(app_root, 'sass'),
    cssDest: build_root
  }
};
