const fs = require('fs');

fs.readdirSync(__dirname).forEach(function(file) {
  if(file.match(/.+\.js$/) && file !== 'index.js') {
    require(`./${file}`);
  }
});
