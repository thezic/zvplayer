const gulp = require('gulp');
const path = require('path');

const config = require('../config');

gulp.task('watch', function() {
  //gulp.watch(path.join(config.path.css, '**/*.css'), ['css']);
  gulp.watch(path.join(config.path.sass, '**/*.{sass,scss}'), ['styles']);
});
