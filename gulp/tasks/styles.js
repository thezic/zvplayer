const gulp = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const path = require('path');

var config = require('../config');

gulp.task('styles', function() {
  gulp.src(path.join(config.path.sass, '**/*.scss'))
        .pipe(sourcemaps.init())
        .pipe(sass({
          outputStyle: 'compressed'
        }).on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(config.path.cssDest));
});
