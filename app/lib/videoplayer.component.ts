import {Component, Input} from 'angular2/core';
import {ViewChild} from 'angular2/core';
import {File} from './file';

@Component({
    selector: 'zv-video',
    template: `
        <div *ngIf="file">
            <video [src]="file.fullPath()" #videoplayer
                    preload="auto"></video>
            <p>playing: {{ file.fullPath() }}</p>
        </div>
    `
})
export class VideoPlayerComponent {
    private _videoFile: File;

    @ViewChild('videoplayer') _videoplayer;

    @Input()
    public set file(v : File) {
        this._videoFile = v;
    }

    public get file() : File {
        console.log(this._videoFile);
        return this._videoFile;
    }

    private get _playerEl() : HTMLVideoElement {
        return this._videoplayer.nativeElement;
    }

    play() {
        if(this.file) {
            console.log("Start playing:", this._videoplayer);
            // this._videoplayer.nativeElement.play();
            this._playerEl.play()
        }
    }

    pause() {
        if(this.file){
            this._playerEl.pause();
        }
    }

    playPause() {
        if(!this.file)
            return;

        if(this._playerEl.paused)
            this.play();
        else
            this.pause();
    }

    setTime(time: number) {
        if(this.file) {
           this._playerEl.currentTime = time;
        }
    }

}
