import {Component, Input, OnInit, OnChanges, Output, EventEmitter} from 'angular2/core';
import {sep as path_sep} from 'path';

@Component({
    selector: 'clickable-path',
    template: `
        <span *ngFor="#part of parts; #idx = index">
            <a href="#" (click)="select(idx)">{{ part }}</a> /
        </span>
        `,
})
export class ClickablePathComponent implements OnChanges, OnInit {
    @Input()
    path: string;

    @Output()
    selectPath = new EventEmitter();

    parts: string[];

    select(index) {
        let path = this.parts.slice(0, index+1).join(path_sep);
        this.selectPath.emit(path);
    }

    calculateParts() {
        if(this.path === path_sep) {
            this.parts = [];
            return;
        }

        this.parts = this.path.split(path_sep);
    }

    ngOnInit() {
        //this.calculateParts();
    }

    ngOnChanges(changes) {
        this.calculateParts();
    }
}
