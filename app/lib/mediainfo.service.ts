import {Injectable} from 'angular2/core';
import {MediaInfo, mediainfo} from 'mediainfo';

// import {mediainfo as mi} from 'mediainfo';

@Injectable()
export class MediaInfoService {
    getInfo(file_path): Promise<MediaInfo> {
        function resolver(resolve, reject) {
            try {
                let info = mediainfo(file_path);
                return resolve(info);
            } catch (error) {
                return reject(error);
            }
        }

        return new Promise<MediaInfo>(resolver);
    }
}

