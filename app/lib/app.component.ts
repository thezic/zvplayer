import { Component, ViewChild } from 'angular2/core';
import { FilebrowserComponent } from './filebrowser.component';
import { ChapterbrowserComponent } from './chapterbrowser.component';
import { VideoPlayerComponent } from './videoplayer.component';
import { FileService } from './file.service';
import { IpcService } from './ipc.service';
import { MediaInfoService } from './mediainfo.service';
import { File } from './file';

@Component({
    selector: 'zvplayer',
    templateUrl: 'assets/zvplayer.html',
    directives: [FilebrowserComponent, ChapterbrowserComponent, VideoPlayerComponent],
    providers: [FileService, MediaInfoService, IpcService],
})
export class AppComponent {
    currentPreviewFile: File;
    currentFilePlaying: File;

    @ViewChild(VideoPlayerComponent)
    private _videoPlayer : VideoPlayerComponent;

    constructor(private _ipc: IpcService) {

    }

    previewFile(file: File) {
        this.currentPreviewFile = file;
    }

    startFile(file: File) {
        console.log('execute: ', file);
        this.currentPreviewFile = file;
        this.currentFilePlaying = file;

        this.play();
    }

    startChapter(chapter) {
        console.log('start chapter', chapter);
        if(this.currentFilePlaying === null || this.currentFilePlaying !== this.currentPreviewFile) {
            console.log('first start file');
            this.currentFilePlaying = this.currentPreviewFile;
        }

        this._ipc.send('video-control', {
            'cmd': 'play',
            'file': this.currentFilePlaying,
            'time': chapter.start_sec,
        });

        this._videoPlayer.setTime(chapter.start_sec);
        this.play();
    }

    pausePlay() {
        this._videoPlayer.playPause();
    }

    play() {
        this._videoPlayer.play();
    }
}
