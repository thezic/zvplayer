import {Component, Input, Output, OnChanges, EventEmitter} from 'angular2/core';
import {MediaInfoService} from './mediainfo.service';
import {File} from './file';
import * as _ from 'lodash';

@Component({
    'selector': 'chapter-browser',
    templateUrl: 'assets/chapterbrowser.html'
})
export class ChapterbrowserComponent implements OnChanges {
    @Input() file: File;
    @Output() selectChapter = new EventEmitter();
    chapters = [];

    constructor(private _mediainfoService: MediaInfoService) { }

    ngOnInit() {

    }

    ngOnChanges(changes) {
        if(this.file)
            this._mediainfoService.getInfo(this.file.fullPath()).then((info) => {
                this.chapters = info.chapters;
                console.log(this.chapters);
            }, (error) => {
                this.file = null;
                this.chapters = [];
            });
    }

    onDoubleClickChapter(chapter) {
        this.selectChapter.emit(chapter);
    }

    // @Input()
    // public set file(file: File) {
    //     this._file;
    //     console.log(file);
    // }

    // public get file() { return this._file; }
}
