import {Injectable, NgZone} from 'angular2/core';

// import {ipcRenderer} from 'electron';
const ipc = require('electron').ipcRenderer;


@Injectable()
export class IpcService {
    constructor(private _zone: NgZone) {

    }

    on(channel:string, listener) {
        ipc.on(channel, (...args) => {
            this._zone.run(listener(...args));
        });
    }

    send(channel:string, ...args) {
        ipc.send(channel, ...args);
    }

    removeListener(channel:string, listener) {
        ipc.removeListener(channel, listener);
    }
}
