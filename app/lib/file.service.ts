import {Injectable} from 'angular2/core';
import {File, TFileType} from './file';
import * as fs from 'fs';
import * as path from 'path';
import * as async from 'async';

@Injectable()
export class FileService {
    getFiles(dir_path) : Promise<File[]> {
        if (typeof dir_path === typeof undefined) {
            dir_path = this.getRoot();
        }

        function resolver(resolve, reject) {
            fs.readdir(dir_path, (err, filenames) => {
                if(err)
                    reject(err);

                let pending = filenames.length;
                let files: File[] = [];

                filenames.forEach((filename, index) => {
                    let file_path = path.join(dir_path, filename);
                    fs.stat(file_path, (err, stats) => {
                        files[index] = new File(
                            dir_path,
                            filename,
                            stats.isDirectory() ? TFileType.Directory : TFileType.Other);

                        // Have we handled all files
                        if(--pending === 0 || err) {
                            if(err)
                                reject(err);

                            resolve(files);
                        }
                    });
                });
            });
        }

        return new Promise<File[]>(resolver);
    }

    getRoot() {
        return path.sep;
    }
}
