import * as path from 'path';

export enum TFileType {
    Directory,
    Other,
    Video,
    Sound,
}

export class File {

    constructor(public path: string,
                public filename: string,
                public type: TFileType) {
    }

    isDirectory() {
        return this.type === TFileType.Directory;
    }

    fullPath() {
        return path.join(this.path, this.filename);
    }
}
