import { Component, OnInit, Output, EventEmitter } from 'angular2/core';
import { FileService } from './file.service';
import { ClickablePathComponent } from './clickablepath.component'
import { File } from './file';
import * as _ from 'lodash';

@Component({
    selector: 'file-browser',
    templateUrl: 'assets/filebrowser.html',
    directives: [ClickablePathComponent],
})
export class FilebrowserComponent implements OnInit {
    @Output() selectFile = new EventEmitter();
    @Output() executeFile = new EventEmitter();

    files: File[] = [];
    current_path: string;

    constructor(private _fileService: FileService) { }

    ngOnInit() {
        // this.updatePath(this._fileService.getRoot());
        this.updatePath(__dirname);
    }

    updatePath(path) {
        this.current_path = path;
        this.updateFiles();
    }

    updateFiles() {
        this._fileService.getFiles(this.current_path).then(files => {
            this.files = _.sortBy(files, ['type', 'filename']);
        });
    }

    selectFileOrDirectory(file: File) {
        if(file.isDirectory()) {
            this.updatePath(file.fullPath());
        } else {
            this.selectFile.emit(file);
        }
    }

    OnDoubleClickFile(file: File) {
        if(file.isDirectory())
            return;

        this.executeFile.emit(file);
    }
}
