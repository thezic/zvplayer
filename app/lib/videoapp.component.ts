import {Component, ViewChild, OnInit} from 'angular2/core';
import {IpcService} from './ipc.service';
import {File} from './file';

@Component({
    selector: 'zv-video',
    template: `
        <video class="video-window__player" #videoplayer
                [src]="path"></video>
    `,
    providers: [IpcService]
})
export class VideoAppComponent implements OnInit {
    file: File;
    path = "";

    @ViewChild('videoplayer') _videoplayer;

    private get _playerEl() : HTMLVideoElement {
        return this._videoplayer.nativeElement;
    }

    constructor(private _ipc: IpcService) { }

    ngOnInit() {
         this._ipc.on('video-control', (event, data) => {
            console.log(data);
            let play_file = data.file;
            play_file.__proto__ = File.prototype;

            if(!this.file)
                this.file = play_file;

            if(this.file.fullPath() !==  play_file.fullPath())
                this.file = play_file;

            this.path = this.file.fullPath();

            this._playerEl.play();
            this._playerEl.currentTime = data.time;
        });
    }
}

