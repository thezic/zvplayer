'use strict';

const electron = require('electron');
// import * as electron from 'electron';
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
const ipc = electron.ipcMain;

// const mi = require('mediainfo');

var mainWindow = null;
var videoWindow = null;

function createMainWindow() {
  mainWindow = new BrowserWindow({width: 800, height: 600});
  mainWindow.loadURL('file://' + __dirname + '/main.html');

  var displays = electron.screen.getAllDisplays();
  var externalDisplay = null;

  for(var i in displays) {
    if(displays[i].bounds.x !== 0 || displays[i].bounds.y !== 0) {
      externalDisplay = displays[i];
      break;
    }
  }

  if(externalDisplay) {
    var size = externalDisplay.workAreaSize;
    videoWindow = new BrowserWindow({
      // x: externalDisplay.bounds.x,
      // y: externalDisplay.bounds.y,
      // width: size.width,
      // height: size.height,
      // frame: false
    });
    videoWindow.loadURL('file://' + __dirname + '/video.html');
  }

  app.on('closed', function() {
    mainWindow = null;
    videoWindow = null;
  });
}

app.on('window-all-closed', function() {
  if(process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('ready', createMainWindow);

app.on('activate', function() {
  if(mainWindow === null) {
    createMainWindow();
  }
});

ipc.on('video-control', function(event, data) {
  videoWindow.webContents.send('video-control', data);
  console.log(data);
});
